# AppyData

1. Download Visual Studio Community Edition 2019: https://visualstudio.microsoft.com/downloads/
   You will be prompted with packages to download. Select ASP.NET and web development, .NET desktop development,   
   Data Storage and processing. Note: Mac users may find it easier to do this using a VM.

2. Open up Visual Studio and select "Continue without code" in the bottom right

3. In the toolbar, click View > Terminal to open up a terminal window

4. Navigate to the folder that you saved this project in using 'cd' (e.x. cd C:\...)

5. Type in 'msbuild Project-01\Project-01.csproj' to build the project

6. Type in '.\Project-01\Project-01.sln' to open the project and click the green arrow next to "IIS Express" to run it

7. Access Administrator level permissions with kyrstinkauchak@gmail.com password: Hello1#, Access Moderator level permissions with kauchakm@miamioh.edu, password: Hello123$, for general user permissions create new account on "Register" page

Documentation: https://docs.google.com/document/d/1xIDI2bgS9F3oDKmM5ZGnc6JyFz2GjakYNueDU_Xlr-g/edit?usp=sharing
