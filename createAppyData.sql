/*
AppyData Database
4/6/2021
MZUM TEAM
CSE 201: Prof Stephan Section B
*/

IF DB_ID('AppyData') IS NOT NULL
    DROP DATABASE AppyData
GO

CREATE DATABASE AppyData
GO

USE AppyData
GO

/****** Object:  Table Apps     ******/
CREATE TABLE tblApps(
	appID int primary key IDENTITY(1,1) NOT NULL,
	appName VARCHAR(30) NOT NULL,
	appDescription VARCHAR(200) NOT NULL, 
	appPlatform VARCHAR(50) NOT NULL,
	appOrganization VARCHAR(20) NOT NULL,
	appPrice FLOAT NOT NULL, 
	appVersions VARCHAR(10) NOT NULL, 
	appLink VARCHAR(50) NOT NULL,
	appImage VARCHAR(200) NOT NULL
)
GO

/****** Object:  Table tblTemApps     ******/
CREATE TABLE tblTempApps(
	appID int primary key IDENTITY(1,1) NOT NULL,
	appName VARCHAR(30) NOT NULL,
	appDescription VARCHAR(200) NOT NULL, 
	appPlatform VARCHAR(50) NOT NULL,
	appOrganization VARCHAR(20) NOT NULL,
	appPrice FLOAT NOT NULL, 
	appVersions VARCHAR(10) NOT NULL, 
	appLink VARCHAR(50) NOT NULL,
	appImage VARCHAR(200) NOT NULL
)
GO

/****** Object:  Table tblForum     ******/
CREATE TABLE tblForum(
	forumID int primary key IDENTITY(1,1) NOT NULL,
	forumComment VARCHAR(350) NOT NULL, 
	appID int FOREIGN KEY REFERENCES tblApps(appID)
	)
GO

ALTER TABLE dbo.tblApps
ALTER COLUMN appDescription VARCHAR(350) NOT NULL
GO

ALTER TABLE tblTempApps
ALTER COLUMN appDescription VARCHAR(350) NOT NULL
GO


ALTER TABLE tblApps
ALTER COLUMN appImage VARCHAR(350)  NOT NULL
GO


ALTER TABLE tblTempApps
ALTER COLUMN appImage VARCHAR(350)  NOT NULL
GO

ALTER TABLE dbo.tblApps
ALTER COLUMN appVersions VARCHAR(50)  NOT NULL
GO

ALTER TABLE dbo.tblTempApps
ALTER COLUMN appVersions VARCHAR(50)  NOT NULL
GO

ALTER TABLE dbo.tblApps
ALTER COLUMN appLink VARCHAR(150)  NOT NULL
GO

ALTER TABLE dbo.tblTempApps
ALTER COLUMN appLink VARCHAR(150)  NOT NULL
GO


ALTER TABLE dbo.tblApps
ALTER COLUMN appOrganization VARCHAR(50)  NOT NULL
GO

ALTER TABLE dbo.tblTempApps
ALTER COLUMN appOrganization VARCHAR(50)  NOT NULL
GO

SET IDENTITY_INSERT [dbo].[tblApps] ON
	INSERT INTO [dbo].[tblApps] ([appID], [appName], [appDescription], [appPlatform], [appOrganization], [appPrice], [appVersions], [appLink],[appImage]) VALUES 
	 (1, 'Starbucks', 'Use our mobile app to order ahead Food & Beverages and pay at participating location or to track the Stars and Rewards you have earned.', 'Apple App Store', 'Starbucks Coffee Company', 0.00, '6.6', 'https://apps.apple.com/us/app/starbucks/id331177714','https://foundation.cpp.edu/dining/assets/img/logo/starbucks-logo-400w.jpg')
	,(2, 'Yelp Food Delivery & Services', 'Whether you are looking for a new pizza place, a great coffee shop nearby, Yelp is your local guide for finding the perfect Food & Beverages options.', 'Apple App Store', 'Yelp', 0, '12.83.0', 'https://apps.apple.com/us/developer/yelp/id284910353','https://www.yelpblog.com/wp-content/uploads/2019/03/Find-Us-On-Yelp-Window-Decal.jpg')
	,(3, 'TikTok', 'TikTok is a short-form, video-sharing Social Media app that allows users to create and share 15-second videos on any topic.', 'Apple App Store', 'TikTok Pte. Ltd', 0.00, '18.8.5', 'https://apps.apple.com/ug/app/tiktok/id835599320/', 'https://img.freepik.com/free-vector/tiktok-banner-with-watercolor-splatter_69286-194.jpg?size=338&ext=jpg')
	,(4, 'Netflix','Netflix is a subscription-based streaming service that allows members to watch TV & Movies ad free on an internet-connected device.','Apple App Store', 'Netflix, Inc.', 0.00,'13.21.0', 'https://apps.apple.com/us/app/netflix/id363590051', 'https://pbs.twimg.com/profile_images/966131468761145344/VBbYp3G7_400x400.jpg')
	,(5	,'Instagram','Connect with friends, share what you are up to, or see what is new from others all over the world. Explore and share everything in this Social Media platform.','Apple App Store','Instagram, Inc.', 0.00,'182', 'https://apps.apple.com/us/app/instagram/id389801252', 'https://cdn2.lamag.com/wp-content/uploads/sites/6/2016/05/Insta.jpg'	)
	,(6	,'Snapchat','Snapchat is a popular messaging app that lets users exchange pictures and videos (called snaps) that are meant to disappear after they are viewed','Apple App Store','Snap,Inc.', 0.00,'11.22.0.5', 'https://apps.apple.com/us/app/snapchat/id447188370', 'https://i.pinimg.com/originals/65/a4/24/65a4240ae9174aa1e5f3af541faba57b.jpg')
	,(7	,'ZOOM','Zoom is a cloud-based video communications app that allows you to set up virtual video and audio conferencing (collaborative capabilities via Social Media).','Apple App Store', 'Zoom',0.00,'5.61', 'https://apps.apple.com/us/app/zoom-cloud-meetings/id546505307', 'https://rhspatriotvoice.org/wp-content/uploads/2020/10/zoom-logo1.jpg'	)
	,(8	,'Hulu: Stream movies & TV Shows','With Hulu you can watch TV & Movies, Originals, past seasons, current episodes, and more. Enjoy all your TV in one place with a plan that works for you.',	'Apple App Store','Hulu LLC.', 0.00,'7.22', 'https://apps.apple.com/us/app/hulu-stream-movies-tv-shows/id376510438', 'https://pics.paypal.com/00/c/gifts/us/hulu.jpg'	)
	,(9	,'Spotify: Discover New Music ','Spotify is a digital Music & Podcasts, and video service that gives you access to millions of songs and other content from creators all over the world.','Apple App Store','Spotify Ltd.',0.00,'8.6.14', 'https://apps.apple.com/us/app/pandora-music-podcasts/id284035177', 'https://pyxis.nymag.com/v1/imgs/2b7/b66/dc2752664adfba3b8523d73873c5bf8034-26-spotify.rsquare.w330.jpg')
	,(10,'Pandora:Music & Podcasts',' Pandora is a leading Music & Podcasts discovery platform, providing a highly-personalized listening experience each month.','Apple App Store','Pandora Media, Inc.', 0.00,'2103.1', 'https://apps.apple.com/us/app/spotify-discover-new-music/id324684580', 'https://customercarecontacts.com/wp-content/uploads/2021/02/pandora-symbol.jpg')
SET IDENTITY_INSERT [dbo].[tblApps] OFF																																												
GO																																																																																			

DROP PROC IF EXISTS spGetCommentList 
GO

CREATE PROC spGetCommentList
	@commentID INT = 0
AS BEGIN 
	SET NOCOUNT ON

	SELECT *
	FROM tblComments
	WHERE commentID = @commentID OR @commentID = 0
	ORDER BY commentID
END
GO

DROP PROC IF EXISTS spGetAppList
GO

CREATE PROC spGetAppList
	@appID INT = 0
AS BEGIN 
	SET NOCOUNT ON

	SELECT *
	FROM tblApps
	WHERE appID = @appID OR @appID = 0
	ORDER BY appDescription
END
GO

DROP PROC IF EXISTS spGetAppTempList
GO

CREATE PROC spGetAppTempList
	@appID INT = 0
AS BEGIN 
	SET NOCOUNT ON

	SELECT *
	FROM tblTempApps
	WHERE appID = @appID OR @appID = 0
	ORDER BY appDescription
END
GO

DROP PROC IF EXISTS spGetAppForum
GO

CREATE PROC spGetForum
	@forumID INT = 0,
	@appID INT
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblForum
	WHERE (forumID = @forumID OR @forumID = 0) AND (appID = @appID)
	ORDER By forumID;
END
GO

DROP PROC IF EXISTS spSearchAppListName
GO
CREATE PROC spSearchAppListName
	@appName			VARCHAR(30)
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblApps
	WHERE appName = @appName 
END
GO

DROP PROC IF EXISTS spSearchAppList
GO

	CREATE PROC spSearchAppList
		@appParam    		VARCHAR(30)
	AS BEGIN 
		SET NOCOUNT ON
		SELECT *
		FROM tblApps
		WHERE (appName LIKE @appParam) OR (appOrganization LIKE @appParam)
	END
	GO


DROP PROC IF EXISTS spSortPriceDESC
GO

CREATE PROC spSortPriceDESC
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblApps
	ORDER BY appPrice DESC
END
GO

DROP PROC IF EXISTS spSortPriceASC
GO

CREATE PROC spSortPriceASC
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblApps
	ORDER BY appPrice ASC
END
GO

DROP PROC IF EXISTS spSortAlphaDESC
GO

CREATE PROC spSortAlphaDESC
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblApps
	ORDER BY appName DESC
END
GO

DROP PROC IF EXISTS spSortAlphaAsc
GO

CREATE PROC spSortAlphaASC
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblApps
	ORDER BY appName ASC
END
GO

DROP PROC IF EXISTS spSearchAppListOrganization 
GO

CREATE PROC spSearchAppListOrganization
	@appOrganization			VARCHAR(50)
AS BEGIN 
	SET NOCOUNT ON
	SELECT *
	FROM tblApps
	WHERE appOrganization = @appOrganization
END
GO


CREATE TABLE tblComments(
	commentID		int primary key IDENTITY(1,1) NOT NULL,
	comment			VARCHAR(200)		NOT NULL		
	)
GO

DROP PROC IF EXISTS SP_AddUpdateDelete
GO

CREATE PROCEDURE SP_AddUpdateDelete
	     @appID				int
		,@appName			VARCHAR(30)
		,@appDescription	VARCHAR(350) 
		,@appPlatform		VARCHAR(50)
		,@appOrganization	VARCHAR(50) 
		,@appPrice			FLOAT 
		,@appVersions		VARCHAR(50)  
		,@appLink			VARCHAR(150) 
		,@appImage			VARCHAR(350)
		,@delete			BIT = 0
    AS BEGIN
		IF @delete = 1 BEGIN
		  BEGIN TRY
			DELETE FROM tblTempApps WHERE appID = @appID
			SELECT [errorNum] = 0, [errorMessage] = '', [appID] = 0
		  END TRY BEGIN CATCH
		      SELECT [errorNum] = 1, [errorMessage] = 'CANNOT DELETE PARENT RECORD', [appID] = 0
		  END CATCH
		END ELSE IF @appID = 0  BEGIN
			INSERT INTO tblTempApps(appName, appDescription, appPlatform,
								   appOrganization, appPrice, appVersions, 
								   appLink, appImage) 
			VALUES (@appName, @appDescription, @appPlatform, 
					@appOrganization, @appPrice, @appVersions,
					@appLink,@appImage)
			SELECT [errorNum] = 0, [errorMessage] = '', [appID] = @@IDENTITY
		END ELSE BEGIN
			UPDATE tblTempApps
			SET 
				 appName			=   @appName			
				,appDescription		=   @appDescription	
				,appPlatform		=	@appPlatform		
				,appOrganization	=	@appOrganization	
				,appPrice			=	@appPrice				
				,appVersions		=	@appVersions		
				,appLink			=	@appLink			
				,appImage			=	@appImage				
			WHERE appID = @appID
			SELECT [errorNum] = 0, [errorMessage] = '', [appID] = @appID
		END
	END
	GO

	
DROP PROC IF EXISTS AddUpdateDelete_SP
GO

	CREATE PROCEDURE AddUpdateDelete_SP
	     @appID				int
		,@appName			VARCHAR(30)
		,@appDescription	VARCHAR(350) 
		,@appPlatform		VARCHAR(50)
		,@appOrganization	VARCHAR(50) 
		,@appPrice			FLOAT 
		,@appVersions		VARCHAR(50)  
		,@appLink			VARCHAR(150) 
		,@appImage			VARCHAR(350)
		,@delete			BIT = 0
    AS BEGIN
		IF @delete = 1 BEGIN
		  BEGIN TRY
			DELETE FROM tblTempApps WHERE appID = @appID
			SELECT [errorNum] = 0, [errorMessage] = '', [appID] = 0
		  END TRY BEGIN CATCH
		      SELECT [errorNum] = 1, [errorMessage] = 'CANNOT DELETE PARENT RECORD', [appID] = 0
		  END CATCH
		END ELSE IF @appID = 0  BEGIN
			INSERT INTO tblApps(appName, appDescription, appPlatform,
								   appOrganization, appPrice, appVersions, 
								   appLink, appImage) 
			VALUES (@appName, @appDescription, @appPlatform, 
					@appOrganization, @appPrice, @appVersions,
					@appLink,@appImage)
			SELECT [errorNum] = 0, [errorMessage] = '', [appID] = @@IDENTITY
		END ELSE BEGIN
			UPDATE tblTempApps
			SET 
				 appName			=   @appName			
				,appDescription		=   @appDescription	
				,appPlatform		=	@appPlatform		
				,appOrganization	=	@appOrganization	
				,appPrice			=	@appPrice				
				,appVersions		=	@appVersions		
				,appLink			=	@appLink			
				,appImage			=	@appImage				
			WHERE appID = @appID
			SELECT [errorNum] = 0, [errorMessage] = '', [appID] = @appID
		END
	END
	GO


DROP PROC IF EXISTS SP_AddUpdateDeleteComments
GO

	CREATE PROCEDURE SP_AddUpdateDeleteComments
		 @commentID		    int
		,@comment	        VARCHAR(200) 
		,@delete			BIT = 0
    AS BEGIN
		IF @delete = 1 BEGIN
		  BEGIN TRY
			DELETE FROM tblComments WHERE commentID = @commentID
			SELECT [errorNum] = 0, [errorMessage] = '', [commentID] = 0
		  END TRY BEGIN CATCH
		      SELECT [errorNum] = 1, [errorMessage] = 'CANNOT DELETE PARENT RECORD', [commentID] = 0
		  END CATCH
		END ELSE IF @commentID = 0  BEGIN
			INSERT INTO tblComments(comment) 
			VALUES (@comment)
			SELECT [errorNum] = 0, [errorMessage] = '', [commentID] = @@IDENTITY
		END ELSE BEGIN
			UPDATE tblComments
			SET 
				 comment			=   @comment					
			WHERE commentID = @commentID
			SELECT [errorNum] = 0, [errorMessage] = '', [commentID] = @commentID
		END
	END
	GO

DROP PROC IF EXISTS SPFilterFood
GO

CREATE PROC SPFilterFood
AS BEGIN
SELECT * FROM tblApps
WHERE appDescription LIKE '%Food & Beverages%'
END
GO

DROP PROC IF EXISTS SPFilterTVMov
GO

CREATE PROC SPFilterTVMov
AS BEGIN
SELECT * FROM tblApps
WHERE appDescription LIKE '%TV & Movies%'
END
GO

DROP PROC IF EXISTS SPFilterSocial
GO

CREATE PROC SPFilterSocial
AS BEGIN
SELECT * FROM tblApps
WHERE appDescription LIKE '%Social Media%'
END
GO

DROP PROC IF EXISTS SPFilterMusic
GO

CREATE PROC SPFilterMusic
AS BEGIN
SELECT * FROM tblApps
WHERE appDescription LIKE '%Music & Podcasts%'
END
GO
/*
	DELETE tblTempApps WHERE appID = 11 OR appID = 13



	SELECT * FROM tblApps
	EXEC SP_AddUpdateDelete 1 

	SELECT * FROM tblTempApps

	DELETE  FROM tblApps WHERE NOT (appID = 1 OR appID =2 )

*/

DROP PROC IF EXISTS SP_AddUpdateDeletetblForum
GO

CREATE PROCEDURE SP_AddUpdateDeletetblForum
		 @forumID		    int
		,@forumComment	    VARCHAR(350)
		,@appID				int
		,@delete			BIT = 0
    AS BEGIN
		IF @delete = 1 BEGIN
		  BEGIN TRY
			DELETE FROM tblForum WHERE forumID = @forumID
			SELECT [errorNum] = 0, [errorMessage] = '', [forumID] = 0
		  END TRY BEGIN CATCH
		      SELECT [errorNum] = 1, [errorMessage] = 'CANNOT DELETE PARENT RECORD', [forumID] = 0
		  END CATCH
		END ELSE IF @forumID = 0  BEGIN
			INSERT INTO tblForum(forumComment, appID) 
			VALUES (@forumComment, @appID)
			SELECT [errorNum] = 0, [errorMessage] = '', [forumID] = @@IDENTITY
		END ELSE BEGIN
			UPDATE tblForum
			SET 
				 forumComment	    =   @forumComment,
				 appID				=   @appID
			WHERE	forumID = @forumID 
			SELECT [errorNum] = 0, [errorMessage] = '', [forumID] = @forumID
		END
	END
	GO

	------------------------------------------------------------------------------------------------------------------------
	/*
		Creating User Permissions/Log ins
	*/
	-- Create users
CREATE TABLE [dbo].[AspNetUsers] (
    [Id]                   NVARCHAR (128) NOT NULL,
    [Email]                NVARCHAR (256) NULL,
    [EmailConfirmed]       BIT            NOT NULL,
    [PasswordHash]         NVARCHAR (MAX) NULL,
    [SecurityStamp]        NVARCHAR (MAX) NULL,
    [PhoneNumber]          NVARCHAR (MAX) NULL,
    [PhoneNumberConfirmed] BIT            NOT NULL,
    [TwoFactorEnabled]     BIT            NOT NULL,
    [LockoutEndDateUtc]    DATETIME       NULL,
    [LockoutEnabled]       BIT            NOT NULL,
    [AccessFailedCount]    INT            NOT NULL,
    [UserName]             NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex]
    ON [dbo].[AspNetUsers]([UserName] ASC);

--userclaims
	CREATE TABLE [dbo].[AspNetUserClaims] (
    [Id]         INT            IDENTITY (1, 1) NOT NULL,
    [UserId]     NVARCHAR (128) NOT NULL,
    [ClaimType]  NVARCHAR (MAX) NULL,
    [ClaimValue] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserClaims]([UserId] ASC);
	-- Creating ASPNETROLES
	CREATE TABLE [dbo].[AspNetRoles] (
    [Id]   NVARCHAR (128) NOT NULL,
    [Name] NVARCHAR (256) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED ([Id] ASC)
);
GO
-- Net Roles Table
CREATE TABLE [dbo].[AspNetUserRoles] (
    [UserId] NVARCHAR (128) NOT NULL,
    [RoleId] NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED ([UserId] ASC, [RoleId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex]
    ON [dbo].[AspNetRoles]([Name] ASC);

-- For ASPNETUSERLOGINS
CREATE TABLE [dbo].[AspNetUserLogins] (
    [LoginProvider] NVARCHAR (128) NOT NULL,
    [ProviderKey]   NVARCHAR (128) NOT NULL,
    [UserId]        NVARCHAR (128) NOT NULL,
    CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED ([LoginProvider] ASC, [ProviderKey] ASC, [UserId] ASC),
    CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);

GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserLogins]([UserId] ASC);

CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[AspNetUserRoles]([UserId] ASC);

GO
CREATE NONCLUSTERED INDEX [IX_RoleId]
    ON [dbo].[AspNetUserRoles]([RoleId] ASC);



INSERT INTO  dbo.AspNetUsers(id,Email, EmailConfirmed,PasswordHash, SecurityStamp, PhoneNumber, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEndDateUtc, LockoutEnabled, AccessFailedCount,UserName)
VALUES	('2e46dbe4-68e3-4abf-8e78-549d7eec54f5','kauchakm@miamioh.edu',0, 'AMi92Br5pGUiztyb6blquWbL7n/peTgF3SdLl6+wwEC6/ZcYhPxviLNYk/AXeOrkpQ==','d375e16a-b819-4e07-8d8e-17546d94899c', NULL, 0,0, NULL, 1, 0, 'kauchakm@miamioh.edu'),
		('4c623c3b-31ee-409b-8386-e4c660fed564','kyrstinkauchak@gmail.com', 0, 'APh+ufsnQ5Co30zWsMvcndojO6biqaK/A0bUmvNwSr0M+45AstlytXzGbCIlUIZbxA==','ee690986-835c-4fd5-b7ef-76029dbea8b3', NULL, 0,0, NULL, 1, 0, 'kyrstinkauchak@gmail.com')
GO

INSERT INTO dbo.AspNetRoles ([Id],[Name]) VALUES ('36EBA45B-A54A-4C53-9719-169391E92D93','Admin'), ('BCCDCD23-D0D8-4C62-9F2A-4416BBA7EC8C', 'Moderator')
GO

INSERT INTO dbo.AspNetUserRoles(UserId,RoleId) VALUES ('4c623c3b-31ee-409b-8386-e4c660fed564','36EBA45B-A54A-4C53-9719-169391E92D93'),('2e46dbe4-68e3-4abf-8e78-549d7eec54f5','BCCDCD23-D0D8-4C62-9F2A-4416BBA7EC8C')
GO







