﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project_01
{
    public partial class ApplicationRequestStatus : System.Web.UI.Page
    {
        /// <summary>
        /// Loads the application request status page.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int counter = 1;
            foreach (ds.spGetCommentListRow r in (new dsTableAdapters.spGetCommentListTableAdapter()).GetData(0))
            {
                commentLabel.Text += counter + " " + r.comment + "<br>";
                counter++;

            }
        }

    }

}
