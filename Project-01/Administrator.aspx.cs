﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project_01
{
    public partial class Administrator : System.Web.UI.Page
    {
        /// <summary>
        /// Loads adminstrator page.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole("Admin")) Response.Redirect("~/RoleAuthError.aspx");
            lblTemp.Text = "";
            int counter = 0;
            int counter2 = 1;
            foreach (ds.spGetAppTempListRow r in (new dsTableAdapters.spGetAppTempListTableAdapter()).GetData(0))
            {
                lblTemp.Text += "<h3>" + counter2 + ".</h3>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + "Name: " + r.appName + "<br>" + "Description: " + r.appDescription + "<br>" + "Platform:" +  r.appPlatform + "<br>" + "Version: " +  r.appVersions + "<br>" + "Price: " +  r.appPrice + "<br>" + "Organization: " + r.appOrganization + "<br>" + "Link: " + "<a href="+ r.appLink + "'  style='color:blue;' target='_blank'>" + r.appLink + "</a>" + "<br>" + "<br>";

                if (counter == 0)
                {
                    TempAppHiddenID.Value = Convert.ToString(r.appID);
                    TempAppHiddenName.Value = r.appName;
                    TempAppHiddenDescription.Value = r.appDescription;
                    TempAppHiddenOrganization.Value = r.appOrganization;
                    TempAppHiddenPlatform.Value = r.appPlatform;
                    TempAppHiddenVersion.Value = r.appVersions;
                    TempAppHiddenLink.Value = r.appLink;
                    TempAppHiddenPrice.Value = Convert.ToString(r.appPrice);
                    TempAppHiddenImage.Value = r.appImage;
                    /*
                        lblTempAppDescription.Text = r.appDescription;
                        lblTempAppOrganization.Text = r.appOrganization;
                        lblTempAppPlatform.Text = r.appPlatform;
                        lblTempAppVersion.Text = r.appVersions;
                        lblTempAppLink.Text = r.appLink;
                        lblTempAppPrice.Text = Convert.ToString(r.appPrice);
                        lblTempImage.Text = r.appImage;
                    */
                }
                counter++;
                counter2++;

            }

        }

        /*
         * Btn_Click
         * 
         * Utilizes stored procedures SP_AddUpdateDelteTable, AddUPdateDelte_SP, and SP_AddUpdateDeleteComments
         * 
         * Adds to application library
         * Deletes from temp table
         * Adds to comments
         * 
         * Forces a redirect to refresh page
         * 
         */
        /// <summary>
        /// Records if "btn" is clicked and accepts the app request.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void btn_Click(object sender, EventArgs e)
        {
            (new dsTableAdapters.AddUpdateDelete_SPTableAdapter()).GetData(0, TempAppHiddenName.Value, TempAppHiddenDescription.Value, TempAppHiddenOrganization.Value, TempAppHiddenPlatform.Value, Convert.ToDouble(TempAppHiddenPrice.Value), TempAppHiddenVersion.Value, TempAppHiddenLink.Value, TempAppHiddenImage.Value, false);
            (new dsTableAdapters.SP_AddUpdateDeleteTableAdapter()).GetData(Convert.ToInt32(TempAppHiddenID.Value), TempAppHiddenName.Value, TempAppHiddenDescription.Value, TempAppHiddenOrganization.Value, TempAppHiddenPlatform.Value, Convert.ToDouble(TempAppHiddenPrice.Value), TempAppHiddenVersion.Value, TempAppHiddenLink.Value, TempAppHiddenImage.Value, true);
            new dsTableAdapters.SP_AddUpdateDeleteCommentsTableAdapter().GetData(0, CommentDesc.Text, false);
            Response.Redirect(Request.RawUrl, true);
        }

        /*
         * Btn_Click
         * 
         * Utilizes stored procedures SP_AddUpdateDelteTable, and SP_AddUpdateDeleteComments
         * 
         * Adds to comments 
         * Deletes from temp table
         * 
         * Forces a redirect to refresh page
         * 
         */
        /// <summary>
        /// Records if "btnDeny" is clicked and denies the app request.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void btnDeny_Click(object sender, EventArgs e)
        {
            (new dsTableAdapters.SP_AddUpdateDeleteTableAdapter()).GetData(Convert.ToInt32(TempAppHiddenID.Value), TempAppHiddenName.Value, TempAppHiddenDescription.Value, TempAppHiddenOrganization.Value, TempAppHiddenPlatform.Value, Convert.ToDouble(TempAppHiddenPrice.Value), TempAppHiddenVersion.Value, TempAppHiddenLink.Value, TempAppHiddenImage.Value, true);
            new dsTableAdapters.SP_AddUpdateDeleteCommentsTableAdapter().GetData(0, CommentDesc.Text, false);
            Response.Redirect(Request.RawUrl, true);
        }

    }
}

