﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project_01
{
    public partial class RequestApp : System.Web.UI.Page
    {
        /// <summary>
        ///  On page load checks if the user is an Admin or not
        /// </summary>
        /// <param name="sender">The event of clicking on webpage tab</param>
        /// <param name="e">The action that occurs (checking to see if user is an admin in user table)</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.Identity.IsAuthenticated) Response.Redirect("~/Account/Login.aspx");
        }
          /// <summary>
          /// Adds the information requested by the user to the temporary table of apps and refreshes the page
          /// </summary>
          /// <param name="sender">The click event sent in (button click)</param>
          /// <param name="e">The action event that occurs (updating table)</param>
        protected void btnsave_Click(object sender, EventArgs e)
        {
            new dsTableAdapters.SP_AddUpdateDeleteTableAdapter().GetData(0, txtAppName.Text, txtAppDesc.Text, txtPlatform.Text, txtOrganization.Text, Convert.ToDouble(txtPrice.Text), txtVersion.Text, txtURL.Text, txtImage.Text, false);
            Response.Redirect(Request.RawUrl, true);

        }
    }
}
