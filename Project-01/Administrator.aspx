﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Administrator.aspx.cs" Inherits="Project_01.Administrator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section id="main-content">
        <section id="wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <h1>Administration</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <br>
                        <br>
                        <!-- Hidden fields allow the values inputed to update the tempAppList table -->
                        <asp:HiddenField ID="TempAppHiddenID" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenName" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenDescription" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenOrganization" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenPlatform" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenVersion" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenLink" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenPrice" runat="server" />
                        <asp:HiddenField ID="TempAppHiddenImage" runat="server" />


                        <asp:Label ID="lblTemp" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempAppName" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempAppDescription" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempAppOrganization" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempAppPrice" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempAppPlatform" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempAppVersion" runat="server" Text=""></asp:Label>
                        <br>
                        <asp:Label ID="lblTempAppLink" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblTempImage" runat="server" Text=""></asp:Label><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Button Text="Add First Request" ID="btn" CssClass="btn btn-primary" Width="170px" runat="server" OnClick="btn_Click" />
                    </div>
                    <div class="col-md-4">
                        <asp:Button Text="Deny First Request" ID="btnDeny" CssClass="btn btn-primary" Width="170px" runat="server" OnClick="btnDeny_Click" />
                    </div>
                    <div class="col-md-4">
                        <asp:Label Text="Comment Description:" runat="server" />
                        <asp:TextBox runat="server" Enabled="true" TextMode="Multiline" Rows="3" CssClass="form-control input-sm" placeHolder="Example: Starbucks App Denied-Link to application is broken" required="Text" ID="CommentDesc" />
                    </div>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
