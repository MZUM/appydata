using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;


namespace Project_01
{
    public partial class AppDetails : System.Web.UI.Page
    {
        ArrayList idHolder = new ArrayList();

        /// <summary>
        /// Loads the app details page.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Loops adds the text on the page for each row
            foreach (ds.spGetAppListRow r in (new dsTableAdapters.spGetAppListTableAdapter()).GetData(Convert.ToInt32(Request.QueryString.Get("id"))))
            {
                lblAppName.Text = "Name: " + r.appName + "<br>";
                lblAppDescription.Text = "Description: " + r.appDescription + "<br>";
                lblAppOrganization.Text = "Organization: " + r.appOrganization + "<br>";
                lblAppPrice.Text = "Price: " + r.appPrice + "<br>";
                lblAppPlatform.Text = "Platform: " + r.appPlatform + "<br>";  
                lblAppVersion.Text = "Version: " + r.appVersions + "<br>";
                lblAppLink.Text = "Link: " + "<a href=" + r.appLink + "  style='color:blue;' target='_blank'>" + r.appLink + "</a>" + "<br>";
            }

            foreach (ds.spGetForumRow r in (new dsTableAdapters.spGetForumTableAdapter()).GetData(0, Convert.ToInt32(Request.QueryString.Get("id"))))
            {
                LblForum.Text += "Anonymous User (ID#" + r.forumID + "):" + "<br>" + r.forumComment + "<br>" + "<br>";
                idHolder.Add(r.forumID);
            }

           if (!User.Identity.IsAuthenticated)
            {
                TxtBoxComment.Visible = false;
                btnsaveComment.Visible = false;
            }

            if (User.IsInRole("Moderator"))
            {
                LblDelete.Visible = true;
                txtForumID.Visible = true;
                ButtonDelete.Visible = true;
            }

            else if (User.IsInRole("Admin"))
            {
                LblDelete.Visible = true;
                txtForumID.Visible = true;
                ButtonDelete.Visible = true;
            }
            else  
            {
                LblDelete.Visible = false;
                txtForumID.Visible = false;
                ButtonDelete.Visible = false;

            }

        }
        /// <summary>
        /// Records if "btnsaveComment" is clicked and adds a comment.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void btnsaveComment_Click(object sender, EventArgs e)
        {
            (new dsTableAdapters.SP_AddUpdateDeletetblForumTableAdapter()).GetData(0,TxtBoxComment.Text, Convert.ToInt32(Request.QueryString.Get("id")), false);
            Response.Redirect(Request.RawUrl, true);
        }
        /// <summary>
        /// Records if "ButtonDelete" is clicked and deletes a comment.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            int myIndex = (int)idHolder[idHolder.IndexOf(Convert.ToInt32(txtForumID.Text))];
         
            (new dsTableAdapters.SP_AddUpdateDeletetblForumTableAdapter()).GetData(myIndex, TxtBoxComment.Text, Convert.ToInt32(Request.QueryString.Get("id")),true);
            Response.Redirect(Request.RawUrl, true);
        }
    }
}
