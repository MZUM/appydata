﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Project_01
{
    public partial class About : Page
    {
        /// <summary>
        /// Loads the about page.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // Loops to create buttons with their corresponding labels and underlying ID's
                foreach (ds.spGetAppListRow r in (new dsTableAdapters.spGetAppListTableAdapter()).GetData(0))
                {
                    lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='170' width='170'>"
                        + "<br>" + r.appName + "</a><br /><br />";
                }

            }
        }

        /// <summary>
        /// Records if "Button1" is clicked.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, true);

        }


        /*
        protected void btnsave_Click(object sender, EventArgs e)
        {
            foreach (ds.spSearchAppListRow r in (new dsTableAdapters.spSearchAppListTableAdapter()).GetData(searchParameter.Text))
            {
                lblAppList.Text = "";

                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
            }
        }
        */

        /*
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl, true);

        }
        */
       
        /// <summary>
        /// Records if "Button2" is clicked.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";

            foreach (ds.spSortPriceDESCRow r in (new dsTableAdapters.spSortPriceDESCTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
           + "<br>" + r.appName + "</a><br /><br />";
            }
        }

        /// <summary>
        /// Records if "ButtonLH" is clicked.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonLH_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";

            foreach (ds.spSortPriceASCRow r in (new dsTableAdapters.spSortPriceASCTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
           + "<br>" + r.appName + "</a><br /><br />";
            }
        }
        /// <summary>
        /// Records if "ButtonAlphDesc" is clicked and sorts apps alphabetically (descending).
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonAlphDesc_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";

            foreach (ds.spSortAlphaDESCRow r in (new dsTableAdapters.spSortAlphaDESCTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
           + "<br>" + r.appName + "</a><br /><br />";
            }
        }
        /// <summary>
        /// Records if "ButtonAlphaAsc" is clicked and sorts apps alphabetically (ascending).
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonAlphaAsc_Click(object sender, EventArgs e)
        {
                lblAppList.Text = "";
                foreach (ds.spSortAlphaASCRow r in (new dsTableAdapters.spSortAlphaASCTableAdapter()).GetData())
                {
                    lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
                }
        }

        /// <summary>
        /// Records if "buttonsave" is clicked.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void buttonsave_Click(object sender, EventArgs e)
        {
            foreach (ds.spSearchAppListRow r in (new dsTableAdapters.spSearchAppListTableAdapter()).GetData(searchParameter.Text))
            {
                lblAppList.Text = "";
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
            }
        }

        /// <summary>
        /// Records if "ButtonTVMov" is clicked and sorts by app category.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonTVMov_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";
            foreach (ds.SPFilterTVMovRow r in (new dsTableAdapters.SPFilterTVMovTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
            }

        }

        /// <summary>
        /// Records if "ButtonMusic" is clicked and sorts by app category.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonMusic_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";
            foreach (ds.SPFilterMusicRow r in (new dsTableAdapters.SPFilterMusicTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
            }

        }

        /// <summary>
        /// Records if "ButtonSoc" is clicked and sorts by app category.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonSoc_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";
            foreach (ds.SPFilterSocialRow r in (new dsTableAdapters.SPFilterSocialTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
            }
        }

        /// <summary>
        /// Records if "ButtonFood" is clicked and sorts by app category.
        /// </summary>
        /// <param name="sender">The object that raised the event.</param>
        /// <param name="e">The object containing event data.</param>
        protected void ButtonFood_Click(object sender, EventArgs e)
        {
            lblAppList.Text = "";
            foreach (ds.SPFilterFoodRow r in (new dsTableAdapters.SPFilterFoodTableAdapter()).GetData())
            {
                lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "'  class='.btn-primary-outline' style='color:black;'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
               + "<br>" + r.appName + "</a><br /><br />";
            }
        }


        /*
//action event spSearchAppList searches based off of developer 
protected void searchParameter_TextChanged(object sender, EventArgs e)
{
   foreach (ds.spSearchAppListRow r in (new dsTableAdapters.spSearchAppListTableAdapter()).GetData("Dunkin"))
   {
       lblAppList.Text += "<a href='AppDetails.aspx?id=" + r.appID + "' class='btn btn-primary'>" + "<img src ='" + r.appImage + "' height='150' width='150'>"
       + "<br>" + r.appName + "</a><br /><br />";
   }
   Response.Redirect(Request.RawUrl, true);
}

*/
    }
}
