﻿CREATE PROC spGetAppList
	@appID INT = 0
AS BEGIN 
	SET NOCOUNT ON

	SELECT *
	FROM tblApps
	WHERE appID = @appID OR @appID = 0
	ORDER BY appDescription
END

