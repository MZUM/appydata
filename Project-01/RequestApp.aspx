<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequestApp.aspx.cs" Inherits="Project_01.RequestApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<!-- 
    Formatting Inspiration: How To Design a Student Registration Form in asp.net web *Forms*
	Author: RashiCode
	Date: 13, October, 2019
	Code version: 1.0
	Availability:https://youtu.be/RzDPaDHIbWQ    
 --> 
    <section id="main-content">
        <section id="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <div class="col-md-4 col-md-offset-4">
                                <h1>Request Application</h1>
                            </div>
                        </header>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application name:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" CssClass="form-control input-sm" placeHolder="Example Name: Starbucks" ID="txtAppName" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application Description:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" TextMode="Multiline" Rows="3" CssClass="form-control input-sm" placeHolder="Example Description: The Starbucks® app is a convenient way to order ahead for pickup or scan and pay in store." ID="txtAppDesc" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application Platform:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" CssClass="form-control input-sm" placeHolder="Example Platform: App Store" ID="txtPlatform" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application Organization:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" CssClass="form-control input-sm" placeHolder="Example Organization: Starbucks Coffee Company" ID="txtOrganization" />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application Price:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" CssClass="form-control input-sm" placeHolder="Example Price: 0" ID="txtPrice" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application Versions:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" CssClass="form-control input-sm" placeHolder="Example Versions: 6.6" ID="txtVersion" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application URL:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" TextMode="Url" CssClass="form-control input-sm" placeHolder="Example URL: https://apps.apple.com/us/app/starbucks/" ID="txtURL" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1">
                                    <div class="form-group">
                                        <asp:Label Text="Application Image URL:" runat="server" />
                                        <asp:TextBox runat="server" Required="" Enabled="true" TextMode="Url" CssClass="form-control input-sm" placeHolder="Example Image URL:  https://apps.apple.com/us/app/starbucks/" ID="txtImage" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-md-offst-2">
                                <asp:Button Text="Save" ID="btnsave" CssClass="btn btn-primary" Width="170px" runat="server" OnClick="btnsave_Click" />
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
</asp:Content>
