<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FAQ.aspx.cs" Inherits="Project_01.FAQ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h1>Help</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Application Library:</h3>
                <p>
                    On this page is the complete library of uploaded applications. To view an application you can click on either the app icon or app name. There is also a search bar available if you need to find a specific application. You can search by application name or developer. There are also different options for sorting and filtering the applications, inlcuding sorting by alphabetically and by price (ascending and descending), as well as filtering based on if it is in the following categories: food & beverages, music & podcasts, social media, and TV & movies. You can select one of these to change which applications or which order applications are displayed. Only one of these options (search, sort, or filter) can be used at a time.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Appilication Details Page</h3>
                <p>On the left, the details of the selected application are displayed. On the right, the comment section is displayed. If you are logged in, you have the ability to add a comment to the comment section. If you are not logged in, you can view the comments, but are unable to add a comment. All comments have an identification number assigned when the comment is posted. If you are logged in as a moderator (or even administrator), you are able to delete comments by their identification number. The designated moderator has the username: kauchakm@miamioh.edu.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Request Application Page</h3>
                <p>On this page, you can submit a request to add your application to the application library if you are logged into your account. In order for your application request to go through and be approved, please fill out all fields accurately and in the same format as the examples in each text box. Once you have filled out this form, your request is complete and will need to be approved by two administrators before being added to the library. You can check to see if you application has been approved or denied by clicking on the Request Statust page, which contains a log of all the requests.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Administration Page</h3>
                <p>This page is only available to users logged in as administrators. This webpage is not visible in the menubar, save for when the administrator himself is logged into his account that has administrative level permissions. On this page administrators will be able to approve or deny the submitted application requests. When making a decision, an administrator must submit a comment in the correct format explaining why he made the decision in the example format: Starbucks App Denied- The url to the the Apple App Store is incorrect. The administrator username is kyrstinkauchak@gmail.com.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Request Status Page</h3>
                <p>On this page, you can view the submitted decisions and corresponding comments the administrator has made for application requests that have been submitted. If the application is approved, you can go look at the Application Library to see your the newly added application. If the application was denied, see what the issue is, fix the issue, and request the application again.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Login Page</h3>
                <p>On this page you canlog in with an existing account. Usernames correspond to emails. Passwords must be at least 6 chararacters long and have alphabetical characters, at least one number, and at least one special character.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">
                <h3>Register Page</h3>
                <p>On this page you create a new account with a username and password. Usernames correspond to emails. Passwords must be at least 6 characters long and have alphabetical characters, at least one number, and at least one special character</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <h3>Coding Documentation</h3>
                    <a href="https://docs.google.com/document/d/1xIDI2bgS9F3oDKmM5ZGnc6JyFz2GjakYNueDU_Xlr-g/edit?usp=sharing" target="_blank" style="color:blue">Visit our coding documentation by clicking here!</a>
            </div>
        </div>
    </div>
</asp:Content>
