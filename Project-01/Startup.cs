﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Project_01.Startup))]
namespace Project_01
{
    public partial class Startup {
        /// <summary>
        /// Configures the application on startup.
        /// </summary>
        /// <param name="app">The application.</param>
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
