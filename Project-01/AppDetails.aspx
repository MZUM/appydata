﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AppDetails.aspx.cs" Inherits="Project_01.AppDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h1>Application Details</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <br>
                <br>
                <asp:Label ID="lblAppName" runat="server" Text=""></asp:Label><br>
                <asp:Label ID="lblAppDescription" runat="server" Text=""></asp:Label><br>
                <asp:Label ID="lblAppOrganization" runat="server" Text=""></asp:Label><br>
                <asp:Label ID="lblAppPrice" runat="server" Text=""></asp:Label><br>
                <asp:Label ID="lblAppPlatform" runat="server" Text=""></asp:Label><br>
                <asp:Label ID="lblAppVersion" runat="server" Text=""></asp:Label>
                <br>
                <asp:Label ID="lblAppLink" runat="server" Text=""></asp:Label><br>
            </div>
            <div class="col-md-3 col-md-offset-2">
                <h2>Comment Section:</h2>
                <asp:TextBox runat="server" Enabled="true" TextMode="Multiline" Rows="2" CssClass="form-control input-sm" Style="resize: none" placeHolder="Example Description: The Starbucks® app is a convenient way to order ahead for pickup or scan and pay in store." ID="TxtBoxComment" Height="68px" Width="286px" />
                <asp:Label ID="LblForum" runat="server" Text=""></asp:Label>
            </div>
            <div class="col-md-2">
                <br>
                <br>
                <br>
                <asp:Button Text="Save" ID="btnsaveComment" CssClass="btn btn-primary" Width="102px" runat="server" Height="47px" OnClick="btnsaveComment_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
            </div>
            <div class="col-md-3 col-md-offset-3">
                <br>
                <br>
                <br>
                <asp:Label ID="LblDelete" Text="Delete Comment ID:" runat="server" />
                <asp:TextBox runat="server" Enabled="true" CssClass="form-control input-sm" placeHolder="Example ID:1" ID="txtForumID" />
                <br>
            </div>
            <div class="col-md-2">
                <br>
                <br>
                <br>
                <asp:Button Text="DELETE" ID="ButtonDelete" CssClass="btn btn-primary" Width="102px" runat="server" Height="47px" OnClick="ButtonDelete_Click" />
            </div>
        </div>
    </div>
</asp:Content>
