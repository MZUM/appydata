<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Project_01.About" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="col-sm-4 col-sm-offset-4">
            <h1>Application Library</h1>
        </div>
        <br>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-sm-5">
                <h2>Search:</h2>
                <asp:TextBox runat="server" Enabled="true" CssClass="form-control input-sm" ID="searchParameter" Width="280px" />
            </div>
            <div class="col-sm-3">
                <h2>Sort:</h2>
                <asp:Label ID="LblHighLow" runat="server" Text="Price High to Low"></asp:Label>
                <asp:Button ID="Button2" CssClass="btn btn-primary" Width="16px" runat="server" OnClick="Button2_Click" Style="color: royalblue" Height="20px" />
            </div>
            <div class="col-sm-4">
                <h2>Filter</h2>
                <asp:Label ID="LblMusic" runat="server" Text="Music & Podcasts"></asp:Label>
                <asp:Button ID="ButtonMusic" CssClass="btn btn-primary" Width="16px" runat="server" Height="20px" Style="color: royalblue" OnClick="ButtonMusic_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
                <asp:Button ID="btnSave" CssClass="btn btn-success" Width="134px" runat="server" Text="Search" OnClick="buttonsave_Click" Height="32px" />
                <asp:Button ID="Button1" CssClass="btn btn-success" Width="140px" runat="server" Text="Refresh Page" OnClick="Button1_Click" Height="32px" />
            </div>
            <div class="col-sm-3">
                <asp:Label ID="LblLowHigh" runat="server" Text="Price Low to High"></asp:Label>
                <asp:Button ID="ButtonLH" CssClass="btn btn-primary" Width="16px" runat="server" OnClick="ButtonLH_Click" Style="color: royalblue;" Height="20px" />
            </div>
            <div class="col-sm-4">
                <asp:Label ID="Label2" runat="server" Text="Food & Beverages"></asp:Label>
                <asp:Button ID="ButtonFood" CssClass="btn btn-primary" Width="16px" runat="server" Style="color: royalblue" Height="20px" OnClick="ButtonFood_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
            </div>
            <div class="col-sm-3">
                <asp:Label ID="LblAlphaAsc" runat="server" Text="Names A-Z"></asp:Label>
                <asp:Button ID="ButtonAlphaAsc" CssClass="btn btn-primary" Width="16px" runat="server" Height="20px" Style="color: royalblue" OnClick="ButtonAlphaAsc_Click" />
            </div>
            <div class="col-sm-4">
                <asp:Label ID="LblFilterSoc" runat="server" Text="Social Media"></asp:Label>
                <asp:Button ID="ButtonSoc" CssClass="btn btn-primary" Width="16px" runat="server" Height="20px" Style="color: royalblue" OnClick="ButtonSoc_Click" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-5">
            </div>
            <div class="col-sm-3">
                <asp:Label ID="LblAllphaDesc" runat="server" Text="Names Z-A"></asp:Label>
                <asp:Button ID="ButtonAlphaDesc" CssClass="btn btn-primary" Width="16px" runat="server" Style="color: royalblue" OnClick="ButtonAlphDesc_Click" Height="20px" />
            </div>

            <div class="col-sm-4">
                <asp:Label ID="LblFilterMov" runat="server" Text="TV & Movies"></asp:Label>
                <asp:Button ID="ButtonTVMov" CssClass="btn btn-primary rounded-0" Width="16px" runat="server" Height="20px" Style="color: royalblue" OnClick="ButtonTVMov_Click" />
            </div>
        </div>
        <div class="row">
            <asp:Label ID="lblAppList" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblSearchList" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>
