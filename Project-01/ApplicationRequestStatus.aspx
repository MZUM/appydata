﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ApplicationRequestStatus.aspx.cs" Inherits="Project_01.ApplicationRequestStatus" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <h1>App Request Status</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
               <!-- <asp:Label ID="comments" runat="server" Text="Comment Descriptions:"></asp:Label>-->
               <h2>Comments:</h2>
                <br>
                <asp:Label ID="commentLabel" runat="server" Text=""></asp:Label>
                <br>
                <br>
            </div>
        </div>
    </div>
</asp:Content>
